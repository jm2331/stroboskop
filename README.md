# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://jm2331@bitbucket.org/jm2331/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/jm2331/stroboskop/commits/f6b2cf33452ce7b0803dac85596db05558a3f014

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/jm2331/stroboskop/commits/5c3d452ad9ca71839bffc4dbb2b766037c0f248a

Naloga 6.3.2:
https://bitbucket.org/jm2331/stroboskop/commits/96c79f30651af79d5ef2221fad517ddda925d024

Naloga 6.3.3:
https://bitbucket.org/jm2331/stroboskop/commits/fc9afa231656d91082974e6c24c1974cb33f9ce6

Naloga 6.3.4:
https://bitbucket.org/jm2331/stroboskop/commits/9df4ee96d7b56ee30a28a2e6c79177f83d08113d

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/jm2331/stroboskop/commits/1267ef76c208634d42ceeaa0ee072efc1b2adcbe

Naloga 6.4.2:
https://bitbucket.org/jm2331/stroboskop/commits/973fa9ad69cffd1785cf6450e1b08fefc0283086

Naloga 6.4.3:
https://bitbucket.org/jm2331/stroboskop/commits/321dfc18eda5b12bc6e27b19ce8dbd7003ac6f37

Naloga 6.4.4:
https://bitbucket.org/jm2331/stroboskop/commits/c957598be0040ff0d162edde3e79a71816d6699b